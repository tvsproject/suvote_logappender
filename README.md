# vVote LogAppender #

The vVote_LogAppender is a custom log appender for Logback that provides reliable forwarding of logging events to a remote server. It converts the logging event into a custom JSON format before sending the log event to the remote server via either HTTP or HTTPS. It can be configured to send the requests synchronously or asynchronously. It has a reliable file back up system that will write appended logging events to file in the event of a failed send to the server. When a successful send is achieved the backed up logs will be sent to the server. As such, it provides a high level of reliability for sending logging events. Additionally, it will reload, and attempt to send, any disk based logs following a restart. 

It sends period hearbeat messages indicating if there are any logs on the disk and whether it believes the log is tainted. A tainted log occurs when an exception takes place during the logging that may have resulted in a log message being lost.

Further information will be provided shortly.